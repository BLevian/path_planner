#
import numpy as np
import math
from pyquaternion import Quaternion


class Camera:
    def __init__(self, *args):  # (self,IntrM,Cam2Piv,calibquat):
        Mode = args[0]
        self.res = args[1] - 1
        IntrM = args[2]

        self.Cam2BodyMat = np.array(args[3]).reshape((4, 4))
        CalibQuat = args[4]
        print(        "Project_tevel(13)>>", CalibQuat)
        self.CalibQuat = CalibQuat
        #        self.Cam2Pivot = Cam2Piv
        self.width = self.res[0]
        self.height = self.res[1]

        if Mode is 'Sim':
            self.__initSimCam(IntrM)
        if Mode is 'Rs':
            self.__initRsCam(IntrM)
        self.Body2CamMat = np.linalg.inv(self.Cam2BodyMat)

    def __initRsCam(self, IntrM):
        self.Proj2CamMat = np.array(IntrM).reshape(3, 3)
        self.Proj2WorldMat = np.linalg.inv(self.Proj2CamMat)
        self.Deproject = self.Project2CamWorld
        self.Project = self.Project2CamPixel

    def __initSimCam(self, IntrM):
        if IntrM.ndim == 1:
            #        [60,3.1,0.6,1920,1080]
            aspect_ratio = float(self.res[0] + 1) / float(self.res[1] + 1)
            fov_rad = IntrM[0] * np.pi / 180.0
            radhfov = 2 * np.arctan(np.tan(fov_rad / 2) / aspect_ratio) * 180 / np.pi
            self.fov = IntrM[0]  # /(16.0/9.0)*aspect_ratio
            self.far = IntrM[1]
            self.near = IntrM[2]
            #            self.res = IntrM[3:5]-1
            self.ratio = aspect_ratio  # 16.0/9.0
            # self.ratio = self.res[0]/self.res[1]
            c = 1 / math.tan((self.fov * math.pi / 180) / 2)
            x1 = (self.near - self.far) / (2 * self.far * self.near)
            x2 = (self.near + self.far) / (2 * self.far * self.near)
            y1 = (self.near + self.far) / (self.near - self.far)
            y2 = (2 * self.far * self.near) / (self.near - self.far)
            self.Proj2CamMat = np.array([[c / self.ratio, 0, 0, 0], [0, c, 0, 0], [0, 0, y1, y2], [0, 0, -1, 0]])
            self.Proj2WorldMat = np.array([[self.ratio / c, 0, 0, 0], [0, 1 / c, 0, 0], [0, 0, 0, -1], [0, 0, x1, x2]])
            self.Deproject = self.Project2WorldByModel
            self.Project = self.Project2CameraByModel

    def Project2CamWorld(self, Pt, Dist):
        #        Pt = np.flip(Pt,0)
        Pt_aug = np.append(Pt, 1.0)
        #        Pt_aug[1]/= self.width
        #        Pt_aug[0]/= self.height
        World = np.dot(self.Proj2WorldMat, Pt_aug)
        # World= World*Dist/World[2] # For real camera
        WorldCam = World * Dist / World[2]  # For real camera
        #        World = self.PermAxis(WorldCam)
        return WorldCam

    def Project2CamWorld_batch(self, xy_points, distances):
        """

        Args:
            xy_points:
            distances: all values must be > 0.

        Returns:

        """
        K = self.Proj2CamMat
        fx, fy, cx, cy = K[0, 0], K[1, 1], K[0, 2], K[1, 2]
        z = distances
        x = z * ((xy_points[:, 0] - cx) / fx)
        y = z * ((xy_points[:, 1] - cy) / fy)
        cam_points = np.dstack((x, y, z))[0]
        return cam_points

    def ProjectCameraFrame2BodyFrame(self, CamWorld):
        CamWorld_aug = np.append(CamWorld, 1)
        Body = np.dot(self.Cam2BodyMat, CamWorld_aug)
        Body = Body[:3]
        return Body

    def ProjectCameraFrame2BodyFrame_batch(self, CamWorld):
        body_points = np.dot(self.Cam2BodyMat, np.concatenate((CamWorld, np.ones((len(CamWorld), 1))), axis=1).T).T[:,
                      :3]
        return body_points

    def Project2Body(self, Pt, Dist):
        CamWorld = self.Deproject(Pt, Dist)
        Body = self.ProjectCameraFrame2BodyFrame(CamWorld)
        return Body

    def ProjectBodyFrame2Pix(self, Body):
        Body_aug = np.append(Body, 1)
        Body2CamNat = np.linalg.inv(self.Cam2BodyMat)
        camera_axes = np.dot(Body2CamNat, Body_aug)
        pix = self.Project(camera_axes[:3])
        return pix, camera_axes

    def FindRadius_batch(self, CirclesCenters, CirclesD, quat, trans, world_points=None):
        if world_points is None:
            world_points = self.Project2World_batch(CirclesCenters, CirclesD.flatten(), quat, trans)
        Center = world_points
        BorderPts = CirclesCenters[:, :2] + np.concatenate(
            (np.zeros((len(CirclesCenters), 1)), np.expand_dims(CirclesCenters[:, 2], 1)), axis=1)
        border_world_points = self.Project2World_batch(BorderPts, CirclesD.flatten(), quat, trans)
        return np.linalg.norm(Center - border_world_points, axis=1)

    @staticmethod
    def Body2World(BodyPos, Q, Trans):
        pos_quat = Quaternion(Q)
        quat_calib = pos_quat
        World = np.dot(quat_calib.rotation_matrix,BodyPos) + Trans
        return World

    @staticmethod
    def Body2World_batch(body_points, Q, Trans):
        pos_quat = Quaternion(Q)
        world_points = np.dot(pos_quat.rotation_matrix, body_points.T).T + Trans
        return world_points

    @staticmethod
    def World2Body(WorldPos, Q, Trans):
        quat = Quaternion(Q)
        Body = quat.inverse.rotate(WorldPos - Trans)
        return Body

    def Project2World(self, Pt, Dist, Q, Trans):
        BodyPos = self.Project2Body(Pt, Dist)
        World = self.Body2World(BodyPos, Q, Trans)
        return World

    def Project2World_batch(self, Pt, Dist, Q, Trans):
        cam_points = self.Project2CamWorld_batch(Pt, Dist)
        body_points = self.ProjectCameraFrame2BodyFrame_batch(cam_points)
        world_points = self.Body2World_batch(body_points, Q, Trans)
        return world_points

    def Project2CamPixel(self, Pt_in_camera_axes):
        Pix_aug = np.dot(self.Proj2CamMat, Pt_in_camera_axes)
        Pix = Pix_aug[:2] / Pix_aug[2]
        return Pix

    def Project2WorldByModel(self, pix, dst):

        #        print "Project >>> Pos = " +repr(dst)
        Pt_ndc = 2 * pix / self.res - 1
        Pt_ndc = np.append(Pt_ndc, [1, 1])
        Pray = np.dot(self.Proj2WorldMat, Pt_ndc)
        World = Pray * dst
        World = World[0:3]
        World[2] = -World[2]
        # t = World[2]
        # World[2] = -World[1]
        # World[1] = -t
        #       WorldOut = self.Project2World(World,dst,Q,Trans)
        #        pos_quat = Quaternion(Q)
        #        quat_calib = pos_quat*self.CalibQuat
        #        quat_calib_inv = quat_calib.inverse
        #        World=quat_calib_inv.rotate(World)+Trans
        return World

    def ProjectInert2Cam(self, TrgInertPos, Quat, SelfPos):
        pos_quat = Quaternion(Quat) * self.CalibQuat
        BodyPos = pos_quat.inverse.rotate(TrgInertPos - SelfPos)
        # BodyPos = pos_quat.inverse.rotate(TrgInertPos - SelfPos)
        BodyPos = np.append(BodyPos, 1)
        CameraAxesPos = np.dot(self.Body2CamMat, BodyPos)
        CameraAxesPos = CameraAxesPos[:3]
        Pix = self.Project(CameraAxesPos)
        return Pix, CameraAxesPos

    def Project2WorldLoocaly(self, pix, dst, Q, Trans):
        World = self.Project2WorldByModel(pix, dst, Q, Trans) - Trans
        return World

    def Project2CameraByModel(self, pos_in_camera_axes):
        pt_aug = np.append(pos_in_camera_axes, 1)
        pt_clip = np.dot(self.Proj2CamMat, pt_aug)
        p_w_clip = -pt_clip[:3] / (pt_clip[3])
        pix = p_w_clip[:2] / 2 + 0.5  # *np.ones_like(p_w_clip[:2])
        pix[0] = pix[0] * self.width
        pix[1] = (pix[1]) * self.height
        return pix

    def DepthImage2Pointcloud(self, depth_image, return_axis='body', quat=None, trans=None):
        """Transform a depth image into a point cloud with one point for each pixel in the image,
        using the camera transform for a camera centred at cx, cy with a field of view fx, fy.
        Can be transformed to camera axis, body axis or world stationary axis.
        Pixels with depth value of 0 are discarded.

        Args:
            depth_image(ndarray): depth image
            return_axis(str): in { 'camera', 'body', 'world' }
            quat(ndarray): body to world quaternion, needed only if return_axis is 'world'
            trans(ndarray): body to world translation, needed only if return_axis is 'world'

        Returns:
            ndarray Nx3, representing the valid points (non zero depth) in the point cloud,
            in camera / body / world axis.

        """
        valid_axis = ('camera', 'body', 'world')
        if return_axis not in valid_axis:
            raise ValueError('return_axis must be of type str in (camera, body, world)')

        K = self.Proj2CamMat
        comm_parameters_cam2bodyMat = self.Cam2BodyMat

        fx, fy, cx, cy = K[0, 0], K[1, 1], K[0, 2], K[1, 2]
        rows, cols = depth_image.shape
        c, r = np.meshgrid(np.arange(cols), np.arange(rows), sparse=True)
        valid = (depth_image > 0.)

        z = np.where(valid, depth_image, 0.)
        x = np.where(valid, z * ((c - cx) / fx), 0)
        y = np.where(valid, z * ((r - cy) / fy), 0)
        cam_points = np.dstack((x, y, z))
        cam_points = cam_points[valid]

        if return_axis == 'camera':
            return cam_points

        cam_points = np.concatenate((cam_points, np.ones((len(cam_points), 1))), axis=1)
        body_points = np.dot(comm_parameters_cam2bodyMat, cam_points.T).T
        body_points = body_points[..., :3]
        if return_axis == 'body':
            return body_points

        pos_quat = Quaternion(quat)
        world_points = np.dot(pos_quat.rotation_matrix, body_points.T).T + trans

        return world_points

    def PermAxis(self, vin):
        vout = np.copy(vin)
        vout[0] = vin[0]
        vout[1] = -vin[2]
        vout[2] = vin[1]
        return vout

    def check_if_pix_in_frame(self, pix):
        width_condition = pix[0] >= 0 and pix[0] < self.width
        height_condition = pix[1] >= 0 and pix[1] < self.height
        return width_condition and height_condition

    def pix_depth_to_length(self, depth):
        p1 = self.Deproject(np.array([0, 0]), depth)
        p2 = self.Deproject(np.array([1, 1]), depth)
        return np.linalg.norm(p1 - p2) / np.sqrt(2)


if __name__ == "__main__":
    #     res= np.array([1920,1080])
    #     Obj= Camera('Sim',res,np.array([60,3.1,0.6]),np.array([1,0,0,0,0,0,1,0,0,1,0,0,0,0,0,1]))
    #                                    [1.036777534227869  , 0.723249756179616 ,  0.341234576617238]])
    #     Pt = np.array([350.0,520.0])
    #     Pt1 = np.array([520.0,350.0])
    #     Dist = 1.9
    #     Q = np.array([0, 0, 0, -1])
    #     Trans = np.array([1,-1.7,1.2])
    #
    #     W = Obj.Project2WorldByModel(Pt,Dist)
    #     W1 =  Obj.Project2WorldByModel(Pt1,Dist)
    #     W2  = Obj.Project2World(Pt1,Dist,Q,Trans)
    #     P = Obj.ProjectInert2Cam(np.array([-.07,0.18,1.62]),Q,np.array([1,-1.7,1.2]))
    #     #W= Obj.Project2World(np.array([10,10]),100,(1,0,0,0),(1,0,0))
    #     print W,W1,W2
    #     print P
    import rospy
    import comm

    ############################################
    #   Test For Rs camera
    ############################################
    rospy.set_param("vispar/mode", "rs")
    res = np.array([640, 480])
    # Intrin = np.array([628.0,0,339.44,0,634.44,225.99,0,0,1])
    # Intrin = np.array([617.0,0.0,311.0,0,622.4,232.8,0,0,1])
    Intrin = np.array([617.38, 0.0, 316.93, 0, 616.83, 226.87, 0, 0, 1])  # Wizard 121
    # Intrin = np.array([615.201, 0.0, 321.65, 0, 615.192, 235.36, 0, 0, 1])  # Wizard 122

    # Intrin = np.array([615.066,0.0,302.683,0,614.488,245.47,0,0,1])

    Cam2Body = np.array([0.0, 0.0, 1.0, 0.0, -1.0, 0.0, 0.0, -0.0, 0.0, -1.0, 0.0, -0.0, 0.0, 0.0, 0.0, 1.0])
    #     Cam2Body = np.array([ 0.0108 ,  -0.0023 ,   0.9999  ,  0.0000 , -0.9999 ,   0.0079 ,   0.0108 ,   0.0509 , -0.0079  , -1.0000 ,  -0.0023  ,  0.0151, 0.0, 0.0, 0.0, 1.0])
    Cam = Camera('Rs', res, Intrin, Cam2Body, comm.Parameters().CameraP.CalibQuat)
    # Cam = Camera('Rs', res, Intrin, comm.Parameters().CameraP.Cam2BodyMat, comm.Parameters().CameraP.CalibQuat)

    TrgPos2D = np.array([218, 376])
    TrgInert = np.array([-1.15, -2.23, 0.6])
    selfPos = np.array([0.0, 0, -0.02])
    selfQ = [0.62, -0.0, 0.00, -0.79]
    distance_in_camaxes = 1.0
    # body = Cam.Project2Body(TrgPos2D,distance_in_camaxes)
    target_2_cam = Cam.ProjectInert2Cam(TrgInert, selfQ, selfPos)
    CameraAxesPos = Cam.Deproject(TrgPos2D, distance_in_camaxes)
    print(    "CameraAxesPos =", CameraAxesPos)
    BodyPos = Cam.Project2Body(TrgPos2D, distance_in_camaxes)
    print(    "BodyPos = ", BodyPos)
    Pix, CameraAxesPos = Cam.ProjectInert2Cam(TrgInert, selfQ, selfPos)
    World = Cam.Project2World(TrgPos2D, distance_in_camaxes, selfQ, selfPos)
    print(    Pix, World)
############################################
############################################
#   Test For Sim camera
############################################
# rospy.set_param("vispar/mode","sim")
# res=np.array([640.0,480.0])
# SimP = comm.Parameters().SimulationP
# Cam = Camera('Sim',SimP.Resolution,SimP.Intrinsics,SimP.Cam2BodyMat,SimP.CalibQuat)
# TrgPos2D =   np.array([320.0,300.0])
# distance_in_camaxes = 0.84
# selfPos = np.array([-0.5, -2.2, 1.22])
# selfQ = [-0.7, 0.0, 0.0, -0.7]
# Target = [0.0,0.0,1.0]
# cam_from_target= Cam.ProjectInert2Cam(Target,selfQ,selfPos)
# BodyFrame = Cam.ProjectCameraFrame2BodyFrame([0.00101194, 0.12250915, 0.84])
# print "BodyFrame = ", BodyFrame
# camerapos = Cam.Deproject(TrgPos2D,distance_in_camaxes)
# body = Cam.Project2World(TrgPos2D,distance_in_camaxes,selfQ,selfPos)
# World = body
# Pix = Cam.ProjectInert2Cam(World,selfQ,selfPos)
# print "body = ",body
# print "Pix= ",Pix
# print "CameraPos=",camerapos
