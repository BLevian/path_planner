import numpy as np
import trimesh
import time
import copy
import open3d as o3d
import point_cloud_to_mesh as pcl2m
from os import path
from pyquaternion import Quaternion
import sys
import pandas
FAR_mesh = o3d.io.read_triangle_mesh("merlin- assem stl.STL")
FAR_mesh.scale(1.5,np.array([0,0,0]))
FAR_mesh.translate(-FAR_mesh.get_center())
WAGON_mesh = o3d.io.read_triangle_mesh("combo_2.PLY")
WAGON_mesh = WAGON_mesh.simplify_quadric_decimation(target_number_of_triangles=6000)
xl_point = pandas.read_csv('Algo_ points.csv',skiprows=1)
stl_points ={}
for dsc in xl_point['Description']:
    stl_points[dsc] = xl_point[xl_point['Description'].str.contains(dsc)][['X','Y','Z']].to_numpy().squeeze()
print(stl_points)
colormap={1:[100,0,0],2:[0,100,0],3:[0,0,100],4:[100,0,100]}

cable1 = o3d.geometry.TriangleMesh.create_sphere(20).paint_uniform_color(colormap[1])
cable1.translate(stl_points['cable 1'])
landing1 = o3d.geometry.TriangleMesh.create_box(40,40,40).paint_uniform_color(colormap[1])
landing1.translate(stl_points['Landing 1'])
cable2 = o3d.geometry.TriangleMesh.create_sphere(20).paint_uniform_color(colormap[2])
cable2.translate(stl_points['cable 2'])
landing2 = o3d.geometry.TriangleMesh.create_box(40,40,40).paint_uniform_color(colormap[2])
landing2.translate(stl_points['Landing 2'])
cable3 = o3d.geometry.TriangleMesh.create_sphere(20).paint_uniform_color(colormap[3])
cable3.translate(stl_points['cable 3'])
landing3 = o3d.geometry.TriangleMesh.create_box(40,40,40).paint_uniform_color(colormap[3])
landing3.translate(stl_points['Landing 3'])
cable4 = o3d.geometry.TriangleMesh.create_sphere(20).paint_uniform_color(colormap[4])
cable4.translate(stl_points['cable 4'])
landing4 = o3d.geometry.TriangleMesh.create_box(40,40,40).paint_uniform_color(colormap[4])
landing4.translate(stl_points['Landing 4'])

o3d.visualization.draw_geometries([WAGON_mesh, cable1,landing1,cable2,landing2,cable3,landing3,cable4,landing4])