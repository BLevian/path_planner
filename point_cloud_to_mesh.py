import sys
import numpy as np
import open3d as o3d
import time
import project_tevel
import tensorflow as tf
from os import path
import scipy
sys.path.append('/home/adi/workspace/path_planner/rs_grabber')
import VideoRecorder

# transformation_to_body = np.array([[0, 0, 1], [-1, 0, 0], [0, -1, 0]])
# transformation_to_body = np.array([[0, 0, 1], [-1, 0, 0], [0, -1, 0]])
transformation_to_body =np.array ([
[-0.16 , -0.0213 , 0.9869 , 231],
[-0.9865 , 0.0394 ,-0.1591 , 10],
[-0.0355 ,-0.999 , -0.0273 , 58],
[ 0, 0, 0, 1 ]])
pad = lambda x: np.vstack([x, np.ones((1,x.shape[1]))])
unpad = lambda x: x[:-1,:]
transform = lambda x,A: unpad(np.dot(A,pad(x)))
def depth_to_pcl(inverse_intrinsics,dpt):
    # x = np.linspace(0, 1, dpt.shape[1])
    # y = np.linspace(0, 1, dpt.shape[0])
    yv, xv = np.mgrid[0: dpt.shape[0], 0:dpt.shape[1]]
    M = np.stack((xv*dpt, yv*dpt,dpt))
    M = M.reshape(3,dpt.shape[0]*dpt.shape[1])
    return scipy.linalg.blas.sgemm(1.0,inverse_intrinsics,M)

def dpt_frame_to_pcl_object(dpt,intrinscs_matrix,trans,quat):
    pt_cloud = depth_to_pcl(np.linalg.inv(intrinscs_matrix), dpt)
    pt_cloud = transform(pt_cloud,transformation_to_body)
    pt_cloud = project_tevel.Camera.Body2World_batch(pt_cloud.T,quat,trans*1000).T
    pt_cloud = np.dot(np.array([[0,1,0],[0,0,1],[1,0,0]]),pt_cloud)
    valid = np.logical_and(pt_cloud[0, :] > 900, pt_cloud[0, :] < 2500)
    pt_cloud = pt_cloud[:, valid]
    point_cloud = o3d.geometry.PointCloud()
    point_cloud.points = o3d.utility.Vector3dVector(pt_cloud.T)
    point_cloud.estimate_normals(search_param=o3d.geometry.KDTreeSearchParamHybrid(radius=0.1, max_nn=30))
    return point_cloud

def point_cloud_to_mesh(point_cloud):
    poisson_mesh = o3d.geometry.TriangleMesh.create_from_point_cloud_poisson(point_cloud, depth=6, width=0, scale=1.1,
                                                                                 linear_fit=False)[0]
    bbox = point_cloud.get_axis_aligned_bounding_box()
    p_mesh_crop = poisson_mesh.crop(bbox)

    return p_mesh_crop
if __name__ == '__main__':
    vid = VideoRecorder.Read_RGBD_Video('vid_200916_130606','/home/adi/workspace/path_planner/assets/video_3d')
    rgb,dpt,t = vid.get_rgb_depth_header()
    # dpt = dpt/1000.0
    intrinsics = vid.color_intrinsics_matrix
    trans = np.array([0,0,1])
    quat = np.array([-0.7,0,0,-0.7])
    point_cloud = dpt_frame_to_pcl_object(dpt,intrinsics,trans,quat)
    o3d.visualization.draw_geometries([point_cloud])
    ############3%%%%%%%
    # distances = pcd.compute_nearest_neighbor_distance()
    # avg_dist = np.mean(distances)
    # radius = 1.5 * avg_dist
    #
    # mesh = o3d.geometry.TriangleMesh.create_from_point_cloud_ball_pivoting(
    #            pcd,
    #            o3d.utility.DoubleVector([radius, radius * 2]))
    ###############33%%%%%%%%%%%%
    tic = time.time()
    mesh_cloud = point_cloud_to_mesh(point_cloud)
    toc = time.time()
    print(toc-tic)
    o3d.visualization.draw_geometries(mesh_cloud)
    print('stop')