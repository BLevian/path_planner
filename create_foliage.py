import point_cloud_to_mesh as pcl2m
import sys
from main import show_mutual_position
import numpy as np
import open3d as o3d


sys.path.append('/home/adi/workspace/path_planner/rs_grabber')
import VideoRecorder
def update_pointcloud_given_mesh(mesh_in,dpt,intrinsics_matrix,pos1,quat1,):
    try:
        mesh_1 = mesh_in.sample_points_uniformly(10000)
    except:
        mesh_1 = mesh_in
    pcl1 = pcl2m.dpt_frame_to_pcl_object(dpt, intrinsics_matrix, pos1, quat1)
    s = pcl1 + mesh_1
    mesh_out = mesh_in + pcl2m.point_cloud_to_mesh(s)
    return mesh_out
q = np.array([-0.7,0,0,-0.7])
T  = np.array([0,.0,0])
vid = VideoRecorder.Read_RGBD_Video('vid_200924_063640', '/home/adi/workspace/path_planner/assets/video_3d')
mesh_foliage = o3d.geometry.TriangleMesh()
frame_no = [200,300,400,500,600]
rgb, dpt, t, pos, quat = vid.get_rgb_depth_by_frame(frame_no[0])
point_cloud = pcl2m.dpt_frame_to_pcl_object(dpt, vid.color_intrinsics_matrix, pos, quat)
mesh_foliage = pcl2m.point_cloud_to_mesh(point_cloud)

for no in frame_no:

    rgb, dpt, t, pos, quat = vid.get_rgb_depth_by_frame(no)
# rgb1, dpt1, t, pos1, quat1 = vid.get_rgb_depth_by_frame(400)
    point_cloud = pcl2m.dpt_frame_to_pcl_object(dpt,vid.color_intrinsics_matrix,pos,quat)
    mesh_foliage = update_pointcloud_given_mesh(mesh_foliage, dpt, vid.color_intrinsics_matrix, pos, quat)

# mesh_foliage = pcl2m.point_cloud_to_mesh(point_cloud)
show_mutual_position(T,q,mesh_foliage)
