import fcl
import numpy as np
import trimesh
import time
import copy
import open3d as o3d
import point_cloud_to_mesh as pcl2m
from os import path
from pyquaternion import Quaternion
import sys
# sys.path.append('/home/adi/workspace/path_planner/rs_grabber')
# import VideoRecorder
far_plan = "merlin- assem stl.STL"
wagon_plan = "Wagon.STL"
class Mesh:
    def __init__(self,plan):
        self.mesh = o3d.io.read_triangle_mesh(plan)
        self.mesh: o3d.geometry.TriangleMesh
        self.mesh.translate(-self.mesh.get_center())
        # self.WAGON_mesh = o3d.io.read_triangle_mesh(wagon_plan)
        # self.WAGON_mesh.translate(-WAGON_mesh.get_center() - np.array([0., 600, 0]))
        # o3d.visualization.draw_geometries([WAGON_mesh,FAR_mesh])

        # self.end_of_arm_body = np.array([0.5, 0, 0])
class Collider:
    def __init__(self,mesh):
        self.bvh_model  = self.read_mesh_open3d(mesh)
        self.collision_model = fcl.CollisionObject(self.bvh_model)

    def read_mesh_open3d(mesh):
        verts = np.asarray(mesh.vertices)
        tris = np.asarray(mesh.triangles)
        m = fcl.BVHModel()
        m.beginModel(len(verts), len(tris))
        m.addSubModel(verts, tris)
        m.endModel()
        return m


class Configuration:
    def __init__(self,intrinsics,cam2body_mat):
        self.far_mesh = Mesh(far_plan)
        self.wagon_mesh = Mesh(wagon_plan)
        self.foliage_mesh = None
        self.intrinsics = intrinsics
        self.cam2body = cam2body_mat

    def frame_to_mesh(self,dpt,pos,quat):
        self.foliage_pc = pcl2m.dpt_frame_to_pcl_object(dpt,self.intrinsics, pos, quat)
        mesh_foliage = pcl2m.point_cloud_to_mesh(self.foliage_pc)
        fcl_foliage_obj = read_mesh_open3d(mesh_foliage)
        fcl_foliage = fcl.CollisionObject(fcl_foliage_obj)
