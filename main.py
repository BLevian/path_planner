import fcl
import numpy as np
import trimesh
import time
import copy
import open3d as o3d
import point_cloud_to_mesh as pcl2m
from os import path
from pyquaternion import Quaternion
import sys
import pandas
sys.path.append('/home/adi/workspace/path_planner/rs_grabber')
import VideoRecorder

def read_mesh_trimesh(mesh ):
    verts = mesh.vertices
    tris = mesh.faces
    m = fcl.BVHModel()
    m.beginModel(len(verts), len(tris))
    m.addSubModel(verts, tris)
    m.endModel()
    return m
def read_mesh_open3d(mesh):
    verts = np.asarray(mesh.vertices)
    tris = np.asarray(mesh.triangles)
    m = fcl.BVHModel()
    m.beginModel(len(verts), len(tris))
    m.addSubModel(verts, tris)
    m.endModel()
    return m
def Body2World(BodyPos,Q,Trans):#TODO:
    pos_quat = Quaternion(Q)
    quat_calib = pos_quat
    quat_calib_inv = quat_calib
    World=quat_calib_inv.rotate(BodyPos)+Trans
    return World
def extract_data_from_excel(point_excel_file_name = 'Algo_ points.csv'):
    xl_point = pandas.read_csv('Algo_ points.csv', skiprows=1)


### trimesh
# FAR_mesh = trimesh.load_mesh('/home/adi/Downloads/merlin- assem stl.STL')
# FAR_mesh.apply_translation(-FAR_mesh.centroid)
# WAGON_mesh = trimesh.load_mesh('Wagon.STL')
# WAGON_mesh.apply_translation(-WAGON_mesh.centroid- np.array([0.,500,0.]))
#### open3d
FAR_mesh = o3d.io.read_triangle_mesh("merlin- assem stl.STL")
FAR_mesh = FAR_mesh.simplify_quadric_decimation(target_number_of_triangles=20000)
# voxel_size = max(FAR_mesh.get_max_bound() - FAR_mesh.get_min_bound()) / 16
# FAR_mesh= FAR_mesh.simplify_vertex_clustering(
#     voxel_size=voxel_size,
#     contraction=o3d.geometry.SimplificationContraction.Average)
FAR_mesh.scale(1.5,np.array([0,0,0]))
FAR_mesh:o3d.geometry.TriangleMesh
FAR_mesh.translate(-FAR_mesh.get_center())
# R = FAR_mesh.get_rotation_matrix_from_quaternion([-0.7,0,-0.7,0])
# FAR_mesh.rotate(R,center= (0,0,0))
WAGON_mesh = o3d.io.read_triangle_mesh("combo_2.PLY")
# WAGON_mesh = o3d.io.read_triangle_mesh("Assem_combo.STL")
WAGON_mesh = WAGON_mesh.simplify_quadric_decimation(target_number_of_triangles=6000)
# WAGON_mesh.translate(WAGON_mesh.get_center())
# voxel_size = max(WAGON_mesh.get_max_bound() - WAGON_mesh.get_min_bound()) / 32
# WAGON_mesh= WAGON_mesh.simplify_vertex_clustering(
#     voxel_size=voxel_size,
#     contraction=o3d.geometry.SimplificationContraction.Average)
xl_point = pandas.read_csv('Algo_ points.csv',skiprows=1)
Cable1 = xl_point[xl_point['Description'].str.contains('Far cable 1')][['X','Y','Z']].to_numpy().squeeze()
Landing_right = np.array([230	,784.8	,1431.63])
Landing_left = np.array([-0,706.4,-963.15])
# Landing_left = np.array([920,1300,560])
# Landing_right = np.array([920,1300,2900])
print("WAGON Center =",WAGON_mesh.get_center())
ROI = o3d.geometry.TriangleMesh.create_sphere(20)
# ROI.translate(Landing2)
ROI.paint_uniform_color([100,0,0])
WAGON_mesh.translate(-Landing_left)
# WAGON_mesh = WAGON_mesh+ROI
# WAGON_mesh.translate(-WAGON_mesh.get_center()-np.array([-200,-100,2000]))
# WAGON_mesh.translate(-WAGON_mesh.get_center()-np.array([0,600,-1200]))
# o3d.visualization.draw_geometries([WAGON_mesh,FAR_mesh])

end_of_arm_body = np.array([0.5,0,0])

# FAR_mesh.show()
# verts = FAR_mesh.vertices
# tris = FAR_mesh.faces
# m = fcl.BVHModel()
# m.beginModel(len(verts), len(tris))
# m.addSubModel(verts, tris)
# m.endModel()
# Collision_obj = trimesh.collision.CollisionManager()
# Collision_obj.add_object('wagon',WAGON_mesh)
# tic = time.time()
# ret = Collision_obj.in_collision_single(FAR_mesh)
# toc = time.time()-tic
FAR_model = read_mesh_open3d(FAR_mesh)
FAR_mesh.translate(-FAR_mesh.get_center())
FAR_mesh.paint_uniform_color([100,0,0])
wagon_model = read_mesh_open3d(WAGON_mesh)
# wagon_model.addSubModel(o3d.geometry.TriangleMesh.create_sphere())
# t = fcl.Transform(q, T)
obj_far = fcl.CollisionObject(FAR_model)
obj_wagon = fcl.CollisionObject(wagon_model)
request = fcl.CollisionRequest()
result = fcl.CollisionResult()
def change_coord(x):
    x_out = np.zeros_like(x)
    x_out[0],x_out[1],x_out[2] = x[1],x[2],x[0]

    # x_out = np.dot(np.array([[0,1,0],[0,0,1],[1,0,0]]),x)
    return x_out
def manipulate_T_q(T,q):
    T_in_mm = T * 1000
    T_in_mm = change_coord(T_in_mm)
    q_v = change_coord(q[1:])
    q_out = np.append(q[0], q_v)
    return T_in_mm,q_out
def collision_check(T,q):
    T,q = manipulate_T_q(T,q)
    t = fcl.Transform(q, T)
    obj = fcl.CollisionObject(FAR_model,t)
    # obj_far_copy = copy.deepcopy(obj_far)
    # obj_far.setTransform(t)
    # obj_far_translated =fcl.CollisionObject(FAR_model,t)
    ret = fcl.collide(obj, obj_wagon, request, result)
    return ret
def distance_check_with(T,q):
    T,q = manipulate_T_q(T,q)
    t = fcl.Transform(q, T)
    FAR_obj = fcl.CollisionObject(FAR_model, t)
    end_of_arm_inert = Body2World(end_of_arm_body,q,T)
    request = fcl.DistanceRequest()
    result = fcl.DistanceResult()
    request.enable_nearest_points = True
    request.gjk_solver_type =fcl.GJKSolverType.GST_INDEP
    ret = fcl.distance(FAR_obj, fcl_foliage, request, result)
    # ?col = collision_check(T,q)
    if ret==0:
        return  -np.linalg.norm(end_of_arm_inert-result.nearest_points[1])
    else :
        return ret

    return ret
# scene  = trimesh.Scene()
# scene.add_geometry(WAGON_mesh)
# def show_posture(q,T):
#     FAR_mesh_translated = FAR_mesh.copy()
#     FAR_mesh_translated.apply_translation(T * 1000)
#     FAR_mesh_translated.apply_transform()

def show_mutual_position(T,q,aux_mesh=[]):
    T,q = manipulate_T_q(T,q)
    FAR_copy = copy.deepcopy(FAR_mesh)
    R = FAR_copy.get_rotation_matrix_from_quaternion(q)
    FAR_copy.rotate(R,center= (0,0,0))
    FAR_copy.translate(T)
    o3d.visualization.draw_geometries([WAGON_mesh, FAR_copy,aux_mesh])

# def collision_check(q,T):
#     tic = time.time()
#     # ret = fcl.collide(obj_far, obj_wagon, request, result)
#     ret = collision_check(T, q)
#     toc = time.time() - tic
#     print("collision = %s , run time = %f" % (ret, toc))
if __name__ == '__main__':

    q = np.array([-0.7,0,0,-0.7])
    T  = np.array([0,.0,0])
    tic  = time.time()
    ret = collision_check(T,q)
    toc = time.time()-tic
    print("collision = %s , run time = %f" % (ret, toc))
    vid = VideoRecorder.Read_RGBD_Video('vid_200924_063640', '/home/adi/workspace/path_planner/assets/video_3d')
    rgb, dpt, t, pos, quat = vid.get_rgb_depth_by_frame(200)
    rgb1, dpt1, t, pos, quat = vid.get_rgb_depth_by_frame(400)

    # dpt = dpt / 1000.0
    # transformation_to_body = np.array([[0, 0, 1], [-1, 0, 0], [0, -1, 0]])
    tic = time.time()

    point_cloud = pcl2m.dpt_frame_to_pcl_object(dpt,vid.color_intrinsics_matrix,pos,quat)
    point_cloud1 = pcl2m.dpt_frame_to_pcl_object(dpt,vid.color_intrinsics_matrix,pos,quat)
    toc = time.time() - tic

    # o3d.visualization.draw_geometries([point_cloud])
    ############3%%%%%%%
    # distances = pcd.compute_nearest_neighbor_distance()
    # avg_dist = np.mean(distances)
    # radius = 1.5 * avg_dist
    #
    # mesh = o3d.geometry.TriangleMesh.create_from_point_cloud_ball_pivoting(
    #            pcd,
    #            o3d.utility.DoubleVector([radius, radius * 2]))
    ###############33%%%%%%%%%%%%
    mesh_foliage = pcl2m.point_cloud_to_mesh(point_cloud)
    fcl_foliage_obj = read_mesh_open3d(mesh_foliage)
    fcl_foliage  = fcl.CollisionObject(fcl_foliage_obj)

    # print(toc - tic)
    show_mutual_position(T,q,mesh_foliage)
    # o3d.visualization.draw_geometries([mesh_cloud])
    ret = distance_check_with(T,q)/1000
    print(ret)
    print('stop')
